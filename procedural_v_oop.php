<?php
// Procedural
function example_new() {
    
    // Nova poznamka
    return array(
        'vars' => array(),
        'test' => 1,
    );
}
//t

function example_set($example, $name, $value) {
    $example['vars'][$name] = $value;
    return $example;
}

function example_get($example, $name) {
    $value = isset($example['vars'][$name]) ? $example['vars'][$name] : null;
    return array($example, $value);
}

$example = example_new();
$example = example_set($example, 'foo', 'hello');
list($example, $value) = example_get($example, 'foo');


// OOP
class Example
{
    private $vars = array();

    public function set($name, $value)
    {
        $this->vars[$name] = $value;
    }

    public function get($name)
    {
        return isset($this->vars[$name]) ? $this->vars[$name] : null;
    }
}

$example = new Example();
$example->set('foo', 'hello');
$value = $example->get('foo');

?>