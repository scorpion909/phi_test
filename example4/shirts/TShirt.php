<?php
namespace Shirt\TShirt;

use Shirt\Shirt;

/**
 * Class TShirt
 * @package Shirt\TShirt
 */
class TShirt extends Shirt {

    /**
     * TShirt constructor.
     * @param string $style
     */
    public function __construct( $style ) {
        $this->set_style( $style );
    }

    /**
     * Get our full shirt options
     * @return array
     */
    public function get_shirt() {
        return [
            $this->style,
            $this->size,
            $this->color,
        ];
    }

}