# DBModel
OOP Model for database management

## HOW TO USE

**require_once('autoloader.php');**

**$dbmodel = new DBModel();**

### WHERE CLAUSE

**$dbmodel->where('field', 'Value', 'operator');**

### SELECT QUERY

@args Argument for returned result
- 1 - * numeric array *
- 2 - * associative array *
- 3 - * associative and numeric array *

**$dbmodel->getSelect($args);**

### GET QUERY TEXT

**$dbmodel->getQueryText();**