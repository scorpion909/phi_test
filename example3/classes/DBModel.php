<?php

class DBModel
{	
    protected $host = "localhost";
    protected $username = "root";
    protected $password = "";
    protected $database = "birds";
    protected $connections;
    protected $qrytxt = "";

    protected $table;
    protected $where = array();

    public function __construct() {
        $this->connections = new mysqli($this->host,$this->username, $this->password, $this->database);

        if ($this->connections->connect_errno) {
            printf("Connect failed: %s\n", $this->connections->connect_error);
            exit();
        }
    }

    public function table($table) {
    	$this->table = $table;

    	return $this;
    }

    public function where($column, $value, $comp = "=") {
    	$this->where[] = "$column $comp '$value'";
    	return $this;
    }

    public function getwhere() {
    	return $this->where;
    }

    public function generateResult($result, $ret) 
    {
        if ($ret == 1) {
            $row = $result->fetch_array(MYSQLI_NUM);
        } elseif ($ret == 2) {
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        } elseif ($ret == 3) {
            $row = $result->fetch_array(MYSQLI_BOTH);
        }
        return $row;
    }

    public function getSelect($ret = 2) {
        
        $this->qrytxt = "SELECT * FROM " . $this->table . " WHERE " . implode(" AND ", $this->where);

        $result = $this->connections->query($this->qrytxt);
        
        return $this->generateResult($result, $ret);
    }

    public function getQueryText() {
        echo "<code>" . $this->qrytxt . "<code>";
    }

    public function __destruct() {
        $this->connections->close();
    }
}