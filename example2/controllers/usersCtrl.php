<?php

require_once('controller.php');

class UsersCtrl extends Controller {

    public function settings() {
        // here is checked if there are things to update and if there are no errors on the inputs
        if ($_POST && (
            ($_POST['email'] == $_SESSION['user']['email'] || !($err = $this->Users->email_err($_POST['email']))) &&
            ($_POST['username'] == $_SESSION['user']['username'] || !($err = $this->Users->username_err($_POST['username']))) &&
            (!isset($_POST['newPassword']) || 
                (!($err =$this->Users->password_err($_POST['newPassword'])) &&
                !($err =$this->Users->similar_passwords_err($_POST['newPassword'], $_POST['repNewPassword']))))
            )) {
                //we check current password
                if ($this->Users->hashPassword($_POST['password']) != $_SESSION['user']['password']) {
                    $this->set('err', 'Current password is not correct');
                    return;
                }
                //if password is good we proceed to requested changes
                if ($_POST['username'] != $_SESSION['user']['username']) {
                    if ($this->Users->update_elem("username=?", $_POST['username'], $_SESSION['user']['id']))
                        $_SESSION['user']['username'] = $_POST['username'];
                }

                if ($_POST['email'] != $_SESSION['user']['email']) {
                    if ($this->Users->update_elem("email=?", $_POST['email'], $_SESSION['user']['id']))
                        $_SESSION['user']['email'] = $_POST['email'];
                }
                
                if (isset($_POST['newPassword']) && $_POST['newPassword'] != $_POST['password']) {
                    $_POST['newPassword'] = hash("md5", $_POST['newPassword']);
                    if ($this->Users->update_elem("password=?", $_POST['newPassword'], $_SESSION['user']['id']))
                        $_SESSION['user']['password'] = $_POST['newPassword'];
                }
                $this->set('success', 'Your information has correctly been saved.');
            }
    }

    public function registration() {
          //if the user has sent the data
        if ($_POST) {
            // We check the inputs
            if (!($err =$this->Users->email_err($_POST['email'])) &&
            !($err =$this->Users->username_err($_POST['username'])) &&
            !($err =$this->Users->password_err($_POST['password'])) &&
            !($err =$this->Users->similar_passwords_err($_POST['password'], $_POST['repPassword']))) {
                //Inputs are good, we proceed to the creation of the user
                $_POST['password'] =$this->Users->hashPassword($_POST['password']); //password hashing to not conserve the clear one
                if ($result = $this->Users->insert_elem(
                'username, password, email', 
                array($_POST['username'], $_POST['password'], $_POST['email']))) {
                //user correctly created, we save it to the session and redirect to home
                $_SESSION["logged"] = true;
                $_SESSION["user"] = $result->fetch_assoc();
                // we set a welcome message in a session variable that will be passed to the home template
                $_SESSION['success'] = "Welcome " . $_POST['username'] . " ! Here will be displayed all the data you store.";
                $this->redirect('/');
                } else
                $this->set('err', 'Couldn\'t create this user');
            } else
            $this->set('err', $err);
        }
    }

    public function login() {
        //check if the user has sent the info
        if ($_POST) {
            $_POST['password'] = hash("md5", $_POST['password']);
            $result =$this->Users->find('*', 'username=? AND password=?', array($_POST['username'], $_POST['password']));
            if ($result)
                $result = $result->fetch_assoc();
            if (!$result) {
                $this->set("err", "Invalid username or password");

            if ($result =$this->Users->find('*', 'username=?', $_POST['username'])) {
                $result = $result->fetch_assoc();
                //if username exists we increment login_attempts
                if ($result['login_attempts'] < 5) {
                $loginAttempts = $result['login_attempts'] + 1;
              $this->Users->update_elem("login_attempts=?", $loginAttempts, $result['id']);
                if ($loginAttempts >= 5) { 
                    //at 5 fail attempts we lock the user for 10 minutes for security reasons : anti bruteforce system
                    $attemptTime = time();
                  $this->Users->update_elem("attempt_time=?", $attemptTime, $result['id']);
                }
                } else {
                if ($result['attempt_time'] < time() - 600) //if user was locked for 10 minutes we reset the attempts nb
                  $this->Users->update_elem("login_attempts=?", 1, $result['id']);
                else
                    $this->set("err", "Too many login attempts. Try again in 10 minutes.");
                }
            }
            // if good creds and not locked by anti bruteforce system we login
            } else if ($result['login_attempts'] < 5 || $result['attempt_time'] < time() - 600) {

                if ($result['login_attempts']) // login success so we reset failed attempts if they exist
                  $this->Users->update_elem("login_attempts=?", 0, $result['id']);

                $_SESSION["logged"] = true;
                $_SESSION["user"] = $result;
                $this->redirect('/');
            }
        }
    }


    
}

?>