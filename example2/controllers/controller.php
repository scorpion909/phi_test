<?php

//parent controller from which extends the other controllers
//controllers are mainly used to manage the connection between the views and the models

class Controller {
    protected $router = null;

    // template name displayed
    protected $view = null;

    // variables defined by the controllers actions which will be accessible in the view
    protected $vars = [];

    protected $authRequired = false;

    public function __construct($model) {
        $modelName = ucfirst($model->getName());
        $this->$modelName = $model;
    }

    protected function setView($view) {
        $this->view = $view;
    }

    protected function setAuthRequired($value) {
        $this->authRequired = $value;
    }

    public function run($action) {
        //by default the view is the action's name
        $this->view = $action;
        $this->$action();
        return [
            'vars' => $this->vars,
            'view' => $this->view,
            'authRequired' => $this->authRequired,
        ];
    }

    //here are setted the variables that will be used in views
    protected function set($key, $value) {
        $this->vars[$key] = $value;
    }

    protected function redirect($location) {
        exit(header("Location: .{$location}"));
    }
}
?>