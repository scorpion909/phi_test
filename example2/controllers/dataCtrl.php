<?php

require_once('controller.php');

class DataCtrl extends Controller {

    public function home() {
        //find all stored data related to current user.id
        $this->set('results', $this->Data->find('*', 'user_id=?', $_SESSION['user']['id']));
    }

    public function edit() {
        $result = null;
        //check if user is the owner of the data, if yes we get it
        if ($_GET && isset($_GET['id']) && $id = $_GET['id'] &&
            $result = $this->Data->find('*', 'id=? AND user_id=?', array($_GET['id'], $_SESSION['user']['id']))) {
            $data = $result->fetch_assoc();
        }
         // here is checked if there are things to update and if there are no errors on the inputs
        if ($_POST && $data && (
            ($_POST['title'] == $data['title'] || !($err = $this->Data->title_err($_POST['title']))) &&
            ($_POST['content'] == $data['content'] || !($err = $this->Data->content_err($_POST['content']))) &&
            ($_POST['tag'] == $data['tag'] || !($err = $this->Data->tag_err($_POST['tag']))) &&
            !($err = $this->Data->file_err()))) {
                //prevention against db incompatible characters
                $_POST['title'] = $this->Data->real_escape_string($_POST['title']);
                $_POST['content'] = $this->Data->real_escape_string($_POST['content']);
                $_POST['tag'] = $this->Data->real_escape_string($_POST['tag']);

                if ($_POST['title'] != $data['title'] && 
                $this->Data->update_elem("title=?", $_POST['title'], $data['id'])) {
                        $data['title'] = $_POST['title'];
                }
                if ($_POST['content'] != $data['content'] &&
                $this->Data->update_elem("content=?", $_POST['content'], $data['id'])) {
                        $data['content'] = $_POST['content'];
                }
                if ($_POST['tag'] != $data['tag'] &&
                $this->Data->update_elem("tag=?", $_POST['tag'], $data['id'])) {
                        $data['tag'] = $_POST['tag'];
                }

                if (!(empty($_FILES['file']['name']))) {
                    $this->Data->remove_file($data);
                    //get uploaded file extension
                    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                    //destination file
                    $dest = getcwd() . "/../private/{$_SESSION['user']['username']}/{$data['id']}.$ext";
                    if (!move_uploaded_file($_FILES['file']['tmp_name'], $dest))
                        return $this->set('err', "Error : Failed to move the uploaded file to {$dest}.");
                    //update filename
                    if (!$this->Data->update_elem("filename=?", $_FILES['file']['name'], $data['id']))
                        return $this->set('err', "Error : Couldn\'t save filename.");  
                }

                if (isset($_POST['redirect'])) { // user wants to be redirected
                    $_SESSION['success'] = "The data \"{$_POST['title']}\" has correctly been edited.";
                    $this->redirect('/');
                }
                else { //user didn't want to get redirect
                    $this->set('success', "The data \"{$_POST['title']}\" has correctly been edited.");
                }
            } else if ($_POST && $data)
            $this->set('err', $err);
        $this->set('data', $data);
    }

    public function delete() {
        //check if user is the owner of the data, if yes we get it
        if ($_GET && isset($_GET['id']) && $id = $_GET['id'] &&
            $result = $this->Data->find('*', 'id=? AND user_id=?', array($_GET['id'], $_SESSION['user']['id'])))
            $result = $result->fetch_assoc();

        // here is checked if there are things to update and if there are no errors on the inputs
        if ($_POST && $result) {
            $this->Data->remove_file($result);
            $this->Data->delete_elem($result['id']);
            $_SESSION['success'] = "The data \"{$result['title']}\" has correctly been deleted.";
            $this->redirect('/');
        }
        $this->set('result', $result);
    }

    public function getFile() {
        // if an error occurs here we print the error template
        $this->setView('error');
        //we check that the f exist and user have the rights to it
        if (isset($_GET['id']) && $result = 
        $this->Data->find('*', 'id=? AND user_id=?', array($_GET['id'], $_SESSION['user']['id']))) {
            // we found the data
            $data = $result->fetch_assoc();
            //get file extension
            $ext = pathinfo($data['filename'], PATHINFO_EXTENSION);
            $path = getcwd() . "/../private/{$_SESSION['user']['username']}/{$data['id']}.$ext";
            if ($data['filename'] && file_exists($path)) {
                // filename exists, we disable the html template because we will print the file
                $this->setView(null);
                $handle = @fopen($path, "rb");

                //set page header
                @header("Cache-Control: no-cache, must-revalidate"); 
                @header("Content-Disposition: attachment; filename= ". $data['filename']);
                @header("Content-type: " . mime_content_type($data['filename']));
                @header("Content-Length: ". filesize($path));
                ob_end_clean(); //required for large files
                fpassthru($handle);

            } else
            return $this->set('err', 'There\'s no file related to this data.');
        } else
            return $this->set('err', 'Error : No file found here related to your user.');
    }

    public function add() {
        //check if the user has sent the info
        if ($_POST) {
            // we check the inputs
            if (!($err = $this->Data->title_err($_POST['title'])) &&
            !($err = $this->Data->content_err($_POST['content'])) &&
            !($err = $this->Data->tag_err($_POST['tag'])) &&
            !($err = $this->Data->file_err())) {
                //prevention against db incompatible characters
                $_POST['title'] = $this->Data->real_escape_string($_POST['title']);
                $_POST['content'] = $this->Data->real_escape_string($_POST['content']);
                $_POST['tag'] = $this->Data->real_escape_string($_POST['tag']);

                //inputs are valid, we can insert
                if ($result = $this->Data->insert_elem(
                'user_id, title, content, tag', 
                array($_SESSION['user']['id'], $_POST['title'], $_POST['content'], $_POST['tag']))) {
                    //data correctly created
                    if (!(empty($_FILES['file']['name']))) {
                        $data = $result->fetch_assoc();
                        //get uploaded file extension
                        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                        //destination file
                        $dest = getcwd() . "/../private/{$_SESSION['user']['username']}/{$data['id']}.$ext";
                        if (!move_uploaded_file($_FILES['file']['tmp_name'], $dest))
                            return $this->set('err', "Error : Failed to move the uploaded file to {$dest}.");
                        //update filename
                        if (!$this->Data->update_elem("filename=?", $_FILES['file']['name'], $data['id']))
                            return $this->set('err', "Error : Couldn\'t save filename.");  
                    }

                    if (isset($_POST['redirect'])) { // user wants to be redirected
                        // we set the success msg in a session variable
                        $_SESSION['success'] = "The data \"{$_POST['title']}\" has correctly been added.";
                        $this->redirect('/');
                    }
                    else { //user didn't want to get redirect
                        $this->set('success',"The data \"{$_POST['title']}\" has correctly been added.");
                    }
                }
            } else
            $this->set('err', $err);
        }
    }
}
?>