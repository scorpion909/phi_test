<?php

class Database {

    private $mysqli = null;

    public function __construct($dbHost = null, $dbUsername, $dbPassword, $dbName) {
        $this->mysqli = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);
        if (!$dbHost) {
            throw new Exception("You didn't set your database credentials.");
        }
        if (!$this->mysqli) {
            throw new Exception("Failed to make a mysql connection : {$this->mysqli->connect_error}");
        }
    }

    // the generic function we'll use to select values
    public function find($model, $fields, $conditions, $values) {
        if (!is_array($values)) // so we can call this function with a value which is not an array
            $values = array($values);

        $table = $model->getName();
        // we use mysqli prepare + bind for security againt sql injections
        if (!($stmt = $this->mysqli->prepare("SELECT {$fields} FROM {$table} WHERE {$conditions};"))) {
            throw new Exception("Failed to prepare SQL statement.");
        }
        // gets the types from the condition with a regex that I made which might evolve
        preg_match_all("/([a-z_-]+)\s*(?:=|<|>|<=|>=|)\s*\?/", $conditions, $matches);
        $types = $model->getTypes($matches[1]);
        // we add the types in the beginning of the array values for bind_param()
        array_unshift($values, $types);
        // we use this fct to be able to call bind_param with an array of params
        if (!(call_user_func_array(array($stmt, 'bind_param'), $this->referenceValues($values)))) {
            throw new Exception("Failed to bind SQL statement.");
        }
        if (!$stmt->execute()) {
            throw new Exception("Failed to find : {$this->mysqli->error}");
        }
        $result = $stmt->get_result();
        $stmt->free_result();
        $stmt->close();
        if (!$result->num_rows)
            return false; // if there's no result
        return $result;
    }

    //the function we'll use to insert values
    public function insert_elem($model, $columns, $values) {
        $table = $model->getName();
        //regexp that will replace each value by '?' for mysqli->prepare
        $prepareVals = preg_replace("/([^,]+)/", "?", $columns);
        if (!($stmt = $this->mysqli->prepare("INSERT INTO {$table} ({$columns}) VALUES ({$prepareVals});"))) {
            throw new Exception("Failed to prepare SQL statement.");
        }
        //split columns by commas, without spaces, and get types string for bind()
        $types = $model->getTypes(array_map('trim', explode(',', $columns)));
        array_unshift($values, $types);
        if (!call_user_func_array(array($stmt, 'bind_param'), $this->referenceValues($values))) {
            throw new Exception("Failed to bind SQL statement.");
        }
        if (!$stmt->execute()) {
            throw new Exception("Failed to insert : {$this->mysqli->error}");
        }
        return $this->find($model, '*', 'id=?', $this->mysqli->insert_id); // return the elem we just inserted
    }

    // the function we'll use to update values
    public function update_elem($model, $changes, $values, $id) {
        if (!is_array($values))
            $values = array($values);
        $table = $model->getName();
        if (!($stmt = $this->mysqli->prepare("UPDATE {$table} SET {$changes} WHERE id={$id};"))) {
            throw new Exception("Failed to prepare SQL statement.");
        }
        //we extract the fields from the changes to get their types
        preg_match_all("/([a-z_-]+)\s*=+\s*\?/", $changes, $matches);
        $types = $model->getTypes($matches[1]);
        array_unshift($values, $types);
        if (!call_user_func_array(array($stmt, 'bind_param'), $this->referenceValues($values))) {
            throw new Exception("Failed to bind SQL statement.");
        }
        if (!$stmt->execute()) {
            throw new Exception("Failed to update : {$this->mysqli->error}");
        }
        return $this->find($model, '*', 'id=?', $id); // return the updated elem
    }

    // the function we'll use to delete values
    public function delete_elem($model, $id) {
        $table = $model->getName();
        if (!$stmt = $this->mysqli->prepare("DELETE FROM {$table} WHERE id=?;")) {
            throw new Exception("Failed to prepare SQL statement.");
        }
        if (!$stmt->bind_param('i', $id)) {
            throw new Exception("Failed to bind SQL statement.");
        }
        if (!$stmt->execute()) {
            throw new Exception("Failed to delete : {$this->mysqli->error}");
        }
        return true; //correctly deleted
    }

    // call_user_func_array() needs 2nd parameter referenced
    private function referenceValues($arr){
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }

    // protect from special characters before db insert
    public function real_escape_string($string) {
        return $this->mysqli->real_escape_string($string);
    }
}

?>