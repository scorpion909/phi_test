<?php

class Router {

    //The routes of the application
    //Each controller in this app has its main Model
    private $routes = array(
        'registration' => array(
            'controller' => 'UsersCtrl',
            'action' => 'registration'),
        'login' => array(
            'controller' => 'UsersCtrl',
            'action' => 'login'),
        'settings' => array(
            'controller' => 'UsersCtrl',
            'action' => 'settings'),
        'home' => array(
            'controller' => 'DataCtrl',
            'action' => 'home'),
        'add' => array(
            'controller' => 'DataCtrl',
            'action' => 'add'),
        'edit' => array(
            'controller' => 'DataCtrl',
             'action' => 'edit'),
        'delete' => array(
            'controller' => 'DataCtrl',
            'action' => 'delete'),
        'getFile' => array(
            'controller' => 'DataCtrl',
            'action' => 'getFile'
        )
    );

    private $defaultModels = array(
        'DataCtrl' => 'Data',
        'UsersCtrl' => 'User'
    );

    // default paths
    private $loggedPath = "home";
    private $unloggedPath = "registration";

    //root lvl starts from public for security reasons
    public function getViewPath($view) {
        return getcwd() . '/../views/' . $view . '.php';
    }

    public function getRoute($path) {
        if ($path == 'logout') { // if user wants to logout
            session_unset(); // clear session
            $path = null; // set null for default template
        }
        // if not specified template, select the default one
        if (!$path)
            $path = (isset($_SESSION["logged"]) ? $this->loggedPath : $this->unloggedPath);
        if (!isset($this->routes[$path])) {
            throw new Exception("Unknown path {$path}");
        }
        $route = $this->routes[$path];
        $route['model'] = $this->defaultModels[$route['controller']];
        return $route;
    }
}

?>