<?php

// enter here your db information
$dbHost = 'localhost:8889'; // example : '127.0.1.1'
$dbUsername = 'root';
$dbPassword = 'root';
$dbName = 'ooptest2';

$mysqli = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

// Display error if connection problem
if ($mysqli->connect_errno) {
    echo "Error: Failed to make a MySQL connection : \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}

//the generic function we'll use to create tables
function create_table($mysqli, $name, $columns) {
    $query = "CREATE TABLE {$name} ({$columns});";
    if (!$mysqli->query($query)) {
        echo "Error on create_table : " . mysqli_error($mysqli) . "\n";
        die;
    }
}

//the generic function we'll use to insert values
function insert_elem($mysqli, $table, $columns, $values) {
    $query = "INSERT INTO {$table} ({$columns}) VALUES ({$values});";
    if (!$mysqli->query($query)) {
        echo "Error on insert_elem : " . mysqli_error($mysqli) . "\n";
        die;
    }
}

//Creating the table users
create_table($mysqli, "users", 
"id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(30) NOT NULL,
password VARCHAR(100) NOT NULL,
email VARCHAR(50),
is_admin TINYINT(1) DEFAULT 0,
login_attempts TINYINT(1) DEFAULT 0,
attempt_time INT(6) UNSIGNED DEFAULT 0,
created TIMESTAMP DEFAULT CURRENT_TIMESTAMP");

//Creating the table data
create_table($mysqli, "data", 
"id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
user_id INT(6) UNSIGNED,
FOREIGN KEY(user_id) REFERENCES users(id),
title VARCHAR(40) NOT NULL,
filename VARCHAR(200) DEFAULT NULL,
content VARCHAR(3000) NOT NULL,
tag VARCHAR(30) NOT NULL,
created TIMESTAMP DEFAULT CURRENT_TIMESTAMP");

$username = "admin"; //your first user username
$password = "admin"; //your first user password
$email = "admin@admin.ad"; //your first user email
$is_admin = 1; //1 if you want your first user to be an admin, 0 if not

//Creating our first user
insert_elem($mysqli, "users", 
"username, password, email, is_admin",
"'{$username}', '" . hash("md5", $password) . "', '{$email}', {$is_admin}");

$userId = $mysqli->insert_id; // the ID of the user we just created

//Creating an example stored data for our user
insert_elem($mysqli, "data",
"user_id, title, content, tag",
"{$userId}, 'The link of google', 'https://google.fr', 'link'");

echo "Script successfully finished.\n";

$mysqli->close();
?>