<?php
    require('config/db.php');
    require('config/router.php');
    require('models/user.php');
    require('models/data.php');
    require('controllers/usersCtrl.php');
    require('controllers/dataCtrl.php');

    session_start();

    //called from public/index.php
    function init() {
        // enter here your db information
        $dbHost = 'localhost:8889'; // example : '127.0.1.1'
        $dbUsername = 'root';
        $dbPassword = 'root';
        $dbName = 'ooptest2';
        // we'll use this class for all interactions with the db
        $db = new Database($dbHost, $dbUsername, $dbPassword ,$dbName);
        
        $router = new Router();

        // getting rooting path setted in public/.htaccess and ask for its location (controller/action)
        $route = $router->getRoute((isset($_GET['htaccess_query']) ? $_GET['htaccess_query'] : ""));

        //each controller is directly related to its main model
        $model = ($route['model'] ? new $route['model']($db) : null);
        $controller = new $route['controller']($model);
        if (!method_exists($controller, $route['action'])) {
            throw new Exception("The action {$route['action']} does not exist for the controller {$route['controller']}.");
        }
            
        $params = $controller->run($route['action']);
        // we get the view path if there is one chosen
        $view = ($params['view'] ? $router->getViewPath($params['view']) : null);
        // check if template exists or display error
        if ($view && !file_exists($view)) {
            throw new Exception("Unknown view {$view}");
        }
        $params['vars']['currentViewName'] = $params['view'];
        $params['vars']['currentViewPath'] = $view;
        return $params['vars'];
    }