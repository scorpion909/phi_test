<?php

//parent model from which extends models used to validate inputs and to interact with the db

class Model {
    //fieldTypes mainly used for mysqli_bind() types argument
    protected $fieldTypes = array();
    protected $tableName = "";
    protected $db = null;

    //consts used by childs to define the types
    const INTEGER = "i";
    const STRING = "s";

    //below the functions used to interact with the db
    public function find($fields, $conditions, $values) {
        return $this->db->find($this, $fields, $conditions, $values);
    }

    public function insert_elem($columns, $values) {
        return $this->db->insert_elem($this, $columns, $values);
    }
    
    public function update_elem($changes, $values, $id) {
        return $this->db->update_elem($this, $changes, $values, $id);
    }

    public function delete_elem($id) {
        return $this->db->delete_elem($this, $id);
    }

    public function getName() {
        return $this->tableName;
    }
    
    //function which converts fields to bind's types
    public function getTypes($fields) {
        $result = "";
        foreach ($fields as $field) {
            if (!isset($this->fieldTypes[$field])) {
                throw new Exception("Unknown field : {$field} for table {$this->tableName}");
            }
            $result .= $this->fieldTypes[$field];
        }
        return $result;
    }

    public function real_escape_string($data) {
        return $this->db->real_escape_string($data);
    }
}
?>