<?php

require_once('model.php');

//data model to validate inputs and to interact with the db

class Data extends Model {

    public function __construct($db, $tableName = "data") {
        //fieldTypes mainly used for mysqli_bind() types argument
        $this->fieldTypes = array(
            'id' => self::INTEGER,
            'user_id' => self::INTEGER,
            'title' => self::STRING,
            'filename' => self::STRING,
            'content' => self::STRING,
            'tag' => self::STRING,
            'created' => self::INTEGER
        );
        $this->db = $db;
        $this->tableName = $tableName;
    }

    //used to remove existing files when removing/updating data, do nothing if there's no file
    public function remove_file($data) {
        if ($data['filename']) {
            $ext = pathinfo($data['filename'], PATHINFO_EXTENSION);
            $path = getcwd() . "/../private/{$_SESSION['user']['username']}/{$data['id']}.$ext";
            if (file_exists($path))
                unlink($path);
        }
    }

    //used to check uploaded files and create userDir where his files are stored
    public function file_err() {
        if (!(empty($_FILES['file']['name']))) {
            if (!($file = $_FILES['file']))
                return 'Error : Couldn\'t get the file';
            if ($file['size'] > 2000000) // 2mb
                return 'Error : Too big file. (Maximum : 2mb)';
            $userDir = getcwd() . "/../private/{$_SESSION['user']['username']}";
            if (!file_exists($userDir) && !mkdir($userDir))
                return 'Error : Couldn\'t create userDir';
        }
        return false;
    }

    public function title_err($title) {
        if (!$title)
            return 'Error : title can\'t be empty';
        if (strlen($title) > 40)
            return 'Error : Maximum 40 characters allowed for the title';
        return false;
    }

    public function content_err($content) {
        if (!$content)
            return 'Error : content can\'t be empty';
        if (strlen($content) > 10000)
            return 'Error : Maximum 10000 characters allowed for the content';
        return false;
    }

    public function tag_err($tag) {
        if (!$tag)
            return 'Error : tag can\'t be empty';
        if (strlen($tag) > 30)
            return 'Error : Maximum 30 characters allowed for the tag';
        return false;
    }
}
?>