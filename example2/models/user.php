<?php

require_once('model.php');

//users model to validate inputs and to interact with the db

class User extends Model {

    public function __construct($db, $tableName = "users") {
        //fieldTypes mainly used for mysqli_bind() types argument
        $this->fieldTypes = array(
            'id' => self::INTEGER,
            'username' => self::STRING,
            'password' => self::STRING,
            'email' => self::STRING,
            'is_admin' => self::INTEGER,
            'login_attempts' => self::INTEGER,
            'attempt_time' => self::INTEGER,
            'created' => self::INTEGER
        );
        $this->tableName = $tableName;
        $this->db = $db;
    }

    public function hashPassword ($password) {
        return hash("md5", $password);
    }

    public function email_err($email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return 'Error : Invalid email.';
        return false;
    }

    public function username_err($username) {
        if (!preg_match('/[a-zA-Z0-9]{1,30}/', $username)){
            return 'Error : Username has to be composed only of letters and/or numbers (max 30 characters).';
        }
        if ($this->find('*', 'username=?', $username)) {
            return 'Error : Username already used.';
        }
        return false;
    }

    public function password_err($password) {
        if (!$password)
            return 'Error : Password can\'t be empty';
        return false;
    }

    public function similar_passwords_err($pass1, $pass2) {
        if ($pass1 != $pass2)
            return 'Error : Passwords are not similar';
        return false;
    }
}

?>