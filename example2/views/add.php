<h1 class="center">Add Data</h1>
<?php if (isset($err)) :?>
  <p class="error"><?php echo $err;?></p>
<?php endif;?>
<?php if (isset($success)) :?>
  <p class="success"><?php echo $success;?></p>
<?php endif;?>
<form action="./add" method="post" enctype="multipart/form-data">
    <div class="form-group">
    <label maxlength="30" for="title">Title:</label>
    <input required="true" name="title" type="text" class="form-control" id="title">
  </div>
  <div class="form-group">
    <label maxlength="1000" for="pwd">Content:</label>
    <textarea required="true" name="content" type="textarea" class="form-control" id="content"></textarea>
  </div>
  <div class="form-group">
    <label maxlength="30" for="tag">Tag:</label>
    <input required="true" name="tag" type="text" class="form-control" id="tag">
  </div>
  <div class="checkbox">
    <label><input name="redirect" type="checkbox" value="true" <?php echo ($_POST && !isset($_POST['redirect']) ? '' : 'checked');?>>Redirect me to home</label>
  </div>
  <div class="form-group">
    <input title="Upload a file" type="file" class="form-control-file" name="file">
  </div>
  <button type="submit" class="btn btn-default">Add</button>
</form>