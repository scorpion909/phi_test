<h1 class="center">Home</h1>
<?php if (isset($success)) :?>
  <p class="success"><?php echo $success;?></p>
<?php endif;?>
<table id="datatables">
	<thead>
		<tr>
			<th>Title</th>
            <th>Content</th>
            <th>Tag</th>
			<th>File</th>
			<th>Created</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($results) while ($data = $results->fetch_assoc()) : ?>
			<tr>
				 <td class="title">
					<?php echo htmlspecialchars($data['title']); ?>
				 </td>
				 <td class="content">
					<?php echo htmlspecialchars($data['content']); ?>
				 </td>
				 <td class="tag">
					<?php echo htmlspecialchars($data['tag']); ?>
                 </td>
				 <td class="file">
				 	<?php if ($data['filename']):?>
					 	<a href="./getFile?id=<?php echo $data['id'];?>"><?php echo $data['filename'];?></a>
					<?php endif;?>
				 </td>
				 <td class="created">
					<?php echo $data['created']; ?>
                 </td>
                 <td>
                     <a title="edit" href="./edit?id=<?php echo $data['id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     <a title="delete" href="./delete?id=<?php echo $data['id'];?>"><i class="fa fa-trash" aria-hidden="true"></i></i></a>
                 </td>
			</tr>
        <?php endwhile; ?>
	</tbody>
</table>

<script>
window.onload = function () {
		$(document).ready( function () {
	    $('#datatables').DataTable({
	    	"pageLength": 40,
	    	"order": [[ 4, "desc" ]],
		});
	});
}
</script>