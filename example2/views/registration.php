<h1 class="center">Sign up</h1>
<?php if (isset($err)) :?>
  <p class="error"><?php echo $err;?></p>
<?php endif;?>
<form action="./registration" method="post">
    <div class="form-group">
    <label for="username">Username:</label>
    <input required="true" name="username" value="<?php if (isset($_POST['username'])) echo $_POST['username'];?>" type="text" class="form-control" id="username">
  </div>
  <div class="form-group">
    <label for="email">Email address:</label>
    <input required="true" name="email" value="<?php if (isset($_POST['email'])) echo $_POST['email'];?>" type="email" class="form-control" id="email">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input required="true" name="password" type="password" class="form-control" id="pwd">
  </div>
  <div class="form-group">
    <label for="reppwd">Repeat Password:</label>
    <input required="true" name="repPassword" type="password" class="form-control" id="reppwd">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>