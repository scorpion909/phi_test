<?php if ($result) :?>
    <h4 class="center">Do you really want to remove the data with the title "<?php echo $result['title'];?>" ?</h4>
    <?php if (isset($err)) :?>
    <p class="error"><?php echo $err;?></p>
    <?php endif;?>
    <?php if (isset($success)) :?>
    <p class="success"><?php echo $success;?></p>
    <?php endif;?>
    <form action="./delete?id=<?php echo $result['id'];?>" method="post">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        <input type="hidden" name="delete" value="1"/>
        <div class="col-xs-6">
            <button type="submit" class="btn btn-default">Yes</button>
        </div>
        <div class="col-xs-6 right">
            <a href="./" class="btn btn-default">No (back)</a>
        </div>
    </div>
    </form>
<?php else:?>
    <p class="error">You don't have the permission to access this page</p>
<?php endif;?>