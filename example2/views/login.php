<h1 class="center">Login</h1>
<?php if (isset($err)) :?>
  <p class="error"><?php echo $err;?></p>
<?php endif;?>
<form action="./login" method="post">
    <div class="form-group">
    <label maxlength="30" for="username">Username:</label>
    <input required="true" name="username" type="text" class="form-control" id="username">
  </div>
  <div class="form-group">
    <label maxlength="20" for="pwd">Password:</label>
    <input required="true" name="password" type="password" class="form-control" id="pwd">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>