<h1 class="center">Settings</h1>
<?php if (isset($err)) :?>
  <p class="error"><?php echo $err;?></p>
<?php endif;?>
<?php if (isset($success)) :?>
  <p class="success"><?php echo $success;?></p>
<?php endif;?>
<form action="./settings" method="post">
    <div class="form-group">
    <label for="username">Username:</label>
    <input required="true" name="username" value="<?php echo $_SESSION['user']['username'];?>" type="text" class="form-control" id="username">
  </div>
  <div class="form-group">
    <label for="email">Email address:</label>
    <input required="true" name="email" value="<?php echo $_SESSION['user']['email'];?>" type="email" class="form-control" id="email">
  </div>
  <?php if (isset($_GET['changePass'])) :?>
    <div class="form-group">
        <label for="newPwd">New Password:</label>
        <input required="true" name="newPassword" type="password" class="form-control" id="newPwd">
    </div>
    <div class="form-group">
        <label for="reppwd">Repeat New password:</label>
        <input required="true" name="repNewPassword" type="password" class="form-control" id="reppwd">
    </div>
    <?php endif;?>
  <div class="form-group">
    <label for="pwd">Current password:</label>
    <input required="true" name="password" type="password" class="form-control" id="pwd">
  </div>
  <button type="submit" class="btn btn-default">Save</button>
</form>
<?php if (!isset($_GET['changePass'])):?>
    <p style="padding-top:10px"><a href="./settings?changePass=1">Change password</a></p>
<?php endif;?>