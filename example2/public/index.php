<?php
    require('../init.php');
    // if there's an internal exception, we'll display the error
    try {
        $vars = init();
        //we transform the vars setted by the controller in an array to variables
        //EXTR_SKIP flag prevents from overwriting existing variables
        extract($vars, EXTR_SKIP);

        //we check if there's success message to show
        if (isset($_SESSION['success'])) {
            $success = $_SESSION['success'];
            unset($_SESSION['success']);
        }
    } catch (Exception $e) {
        $err = $e->getMessage(); 
        $currentViewPath = '../views/error.php';
        $currentViewName = 'error';
    }
?>

<?php if ($currentViewName) :?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Data Backup</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css?/<?php echo filemtime('css/custom.css');?>">
    <!-- adding related css file if it exists -->
    <?php if (file_exists('css/' . $currentViewName . '.css')) :?>
        <link rel="stylesheet" type="text/css" href="css/<?php echo $currentViewName . '.css?' . filemtime('css/' . $currentViewName . '.css');?>">
    <?php endif;?>
  </head>

    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="./">Data Backup</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <?php if (isset($_SESSION["logged"])) :?>
                <li class="<?php if ($currentViewName == 'add') echo 'active';?>"><a href="./add">Add</a></li>
                <li class="<?php if ($currentViewName == 'home') echo 'active';?>"><a href="./">Home</a></li>
                <li class="<?php if ($currentViewName == 'settings') echo 'active';?>"><a title="Settings" href="./settings"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
                <li><a title="Logout" href="./logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
            <?php else:?>
                <li class="<?php if ($currentViewName == 'registration') echo 'active';?>"><a href="./registration">Sign up</a></li>
                <li class="<?php if ($currentViewName == 'login') echo 'active';?>"><a href="./login">Login</a></li>
            <?php endif;?>
        </ul>
        </div><!--/.nav-collapse -->
    </div>
    </nav>

    <div class="container">
        <div class="template">
            <!-- We use exceptions to handle internal errors, if an exception is thrown we display the error -->
            <?php include($currentViewPath);?>
        </div>
    </div><!-- /.container -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <!-- adding related js file if it exists -->
    <?php if (file_exists('js/' . $currentViewName . '.js')) :?>
        <link rel="stylesheet" type="text/css" href="js/<?php echo $currentViewName . '.js?' . filemtime('js/' . $ht_query . '.js');?>">
    <?php endif;?>
    </body>

</html>
<?php endif;?>