https://databackup.alwaysdata.net/

## PHP Data Backup

**I developed this app in PHP native following the MVC architecture. Feel free to take this app as a base and set-up your own project.**

Concerning the aim of the app. This app lets you store files and text data that only you have the access to.
You can create several accounts and each account has its own data.
The accounts are protected by an anti brute-force attack system and the application is secured against SQL injections.
The main page is composed of a filtrable table with all the data you stored.

The web application has been developed with PHP, HTML/CSS, JS/JQuery, BootStrap and Datatables.

<img src="https://raw.githubusercontent.com/neoski/php-data-backup/master/public/img/demoGithub1.png" width="600px"/>
<img src="https://raw.githubusercontent.com/neoski/php-data-backup/master/public/img/demoGithub2.png" width="600px"/>
<img src="https://raw.githubusercontent.com/neoski/php-data-backup/master/public/img/demoGithub3.png" width="600px"/>

### To use the application :

**1/** Clone the repository with git or download it

**2/** Create a MySQL database and set your MySQL information in `script_install_db.php` and `init.php`

**3/** Run the script_install_db.php with php

**4/** Launch the application by using a web server like apache on your localhost or host it wherever where there is a web sever that supports php

**5/** Create an account _(or connect with the default admin:admin user from the script)_ and store the data you want

You can store the text data you need.

Enjoy!
