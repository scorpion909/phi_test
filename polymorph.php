<?php
interface ITvar
{
    public function obsah();
}

class Trojuhelnik implements ITvar
{
    private $a, $b, $c;

    public function __construct($a, $b, $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    public function obsah()
    {
        //výpočet obsahu pomocí Heronova vzorce
        $o = ($this->a + $this->b + $this->c) / 2;
        return sqrt($o * ($o - $this->a) * ($o - $this->b) * ($o - $this->c));
    }
}

class Kruh implements ITvar
{
    private $polomer;

    public function __construct($polomer)
    {
        $this->polomer = $polomer;
    }

    public function obsah()
    {
        return M_PI * $this->polomer * $this->polomer;
    }
}

function vypisInfo(ITvar $tvar)
{
    echo 'Instance třídy ', get_class($tvar), ', obsah: ', round($tvar->obsah(), 2);
}

vypisInfo(new Trojuhelnik(5, 10, 12)); //Instance třídy Trojuhelnik, obsah: 24.54
echo PHP_EOL;
vypisInfo(new Kruh(10)); //Instance třídy Kruh, obsah: 314.16