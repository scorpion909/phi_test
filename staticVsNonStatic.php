<?php

class Student{
    
    public $age;
    static $generation = 2006;

    public function readPublic(){
        return $this->age;
    }

    public static function readStatic(){
        //return $this->age;         // case 1
        //return $student1->age;    // case 2
        return self::$generation;    // case 3

    }
}

$student1 = new Student();
Student::readStatic();