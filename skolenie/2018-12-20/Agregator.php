<?php

require("db/MySQLi.php");

class Agregator {
    
    private $config;
    protected $db;

    /**
     * Agregator constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;

        $this->db = new MysqliDb(
            $this->config['mysql']['host'], 
            $this->config['mysql']['user'], 
            $this->config['mysql']['pw'], 
            $this->config['mysql']['db']
        );
        
    }

    /**
     * Retrieve data from DB
     *
     * @return array
     */
    private function getSourceData() {

        $out = [];

        $sql = 'SELECT '.$this->config['left_col'].' FROM '.$this->config['source_table'].' GROUP BY '.$this->config['left_col'].' ORDER BY '.$this->config['left_col'].' '.$this->config['left_col_order'].';';
        $results = $this->db->rawQuery($sql);

        // Item je premenna z prveho stlpca
        foreach ($results as $item) {

            $loopItem = $item[$this->config['left_col']];

            $selectWhat = [
               'id' ,$this->config['left_col'], $this->config['compare_val'], $this->config['first_row'],
            ];

            // Select na data
            $sql = 'SELECT '.implode(',', $selectWhat).' FROM '.$this->config['source_table'].' WHERE '.$this->config['left_col'].' = "'.$loopItem.'";';
            $out[$loopItem]['data'] = $this->db->rawQuery($sql);


            // Select na statistiky
//            $selectWhat = [
//                'COUNT('.$this->config['first_row'].') as itemCount','SUM('.$this->config['first_row'].') as itemSum',
//                'AVG('.$this->config['first_row'].') as itemAvg', 'MIN('.$this->config['first_row'].') as itemMin',
//                'MAX('.$this->config['first_row'].') as itemMax'
//            ];
//            $sql = 'SELECT '.implode(',', $selectWhat).' FROM '.$this->config['source_table'].' WHERE '.$this->config['left_col'].' = '.$loopItem.';';
//            $out[$loopItem]['stats'] = $this->db->rawQuery($sql);

        }

        return $out;
    }
    
    
    public function agregateValues() {

        // Vyberieme data
        $data = $this->getSourceData();

        // Zmazeme tabulku
        $this->db->rawQuery('DROP TABLE IF EXISTS '.$this->config['final_table'].';');

        // Select na first row data
        $sql = 'SELECT '.$this->config['first_row'].' FROM '.$this->config['source_table'].' GROUP BY '.$this->config['first_row'].' ORDER BY '.$this->config['first_row'].' '.$this->config['first_row_order'].';';
        $firstRowData = $this->db->rawQuery($sql);

        $firstRowSql = '';
        foreach ($firstRowData as $item) {

            $key = $item[$this->config['first_row']];
            if($key == '') {
                $key = 'none';
            }
            $firstRowSql .= ",`".$key."` VARCHAR(255) DEFAULT 0 ";
            $firstRowNames[$key] = [];
        }

        // Create final table
        $sql = "CREATE TABLE `".$this->config['final_table']."` (
            `left_agr` VARCHAR(25)
            ".$firstRowSql."
        )";
        $this->db->rawQuery($sql);

        $dataSorted = [];
        
        // Pripravime data a zapiseme do MyQSL
        foreach ($data as $agregateKey => $values) {

            // Cyklus na vsetky data
            foreach ($values['data'] as $item) {

                $loopKey = $item[$this->config['first_row']];
                if($loopKey == '') {
                    $loopKey = 'none';
                }

                $dataSorted[$agregateKey][$loopKey][] = $item;

            }

        }

        // Cyklenie poloziek a zistenie SUM, AVG, MIN, MAX ..
        foreach ($dataSorted as $agregateKey => $values) {

            foreach ($values as $loopKey => $items) {

                $operationVar = NULL;

                foreach ($items as $singleItem) {

                    // SUM, COUNT, AVG ...
                    switch ($this->config['operation']) {
                        case 'SUM':

                            // SETUJEME RESULT DO JEDNEJ PREMMENNEJ
                            $operationVar += $singleItem[$this->config['compare_val']];

                        break;

                        case 'COUNT':

                            // SETUJEME RESULT DO JEDNEJ PREMMENNEJ
                            $operationVar++;

                        break;
                    }

                }

                // PREMENNA PREPISE JEDNOTLIVE DATA, KTORE SME DOSTALI Z DB ALE UZ ICH NEPOTREBUJEME
                $dataSorted[$agregateKey][$loopKey] = $operationVar;
            }
            
        }

        // Ulozime data do final table
        foreach ($dataSorted as $agregateKey => $values) {

            $colNames = NULL;
            $i = 0;
            foreach ($values as $key => $value) {
                if($i < count($values)) {
                    $colNames .= ',';
                }
                $colNames .= '`'.$key.'`';
                $i++;
            }


            $sql = "INSERT INTO ".$this->config['final_table']." (`left_agr` ".$colNames.") ";

            $colNames = NULL;
            $i = 0;
            foreach ($values as $key => $value) {
                if($i < count($values)) {
                    $colNames .= ',';
                }
                $colNames .= "'".$value."'";
                $i++;
            }

            $sql .= "VALUES ('".$agregateKey."' ".$colNames.")";

            $this->db->rawQuery($sql);

        }
        
        // Vratime result
        return true;
    }

}