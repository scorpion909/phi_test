<?php
function dd($what, $what2 = null, $what3 = null, $die = true) {
    echo "<pre>";
    var_dump($what);
    if($what2) {
        echo "<hr>";
        var_dump($what2);
    }
    if($what3) {
        echo "<hr>";
        var_dump($what3);
    }
    exit;
}

include 'Agregator.php';

// Tvorba kontingencnej tabulky (20.12.2018)

// Konfiguracia 
// Pouzivaju sa RAW SQL query kde parametre nie su escaped
$config = [
    'final_table' => 'kontingent',
    'source_table' => 'test_vs_vyluka',

    // Connect na MySQL
    'mysql' => [
        'host' => '127.0.0.1:8889',
        'user' => 'root',
        'pw' => 'root',
        'db' => 'pharmin_skolenie_18_12_2018'
    ],

    // Stlpce tabulky
    'left_col_order' => 'ASC',
    'left_col' => 'KodLieku',
    'first_row_order' => 'ASC',
    'first_row' => 'KodPzsPredpisujucehoLekara',
    'compare_val' => 'UhradaPoistovne',
    'operation' => 'COUNT'
];

$agregator = new Agregator($config);
$result = $agregator->agregateValues();

echo var_dump($result);