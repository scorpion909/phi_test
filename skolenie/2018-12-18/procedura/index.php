<!DOCTYPE html>
<html>
<head>
    <title>PHP MySQL Stored Procedure Demo 1</title>
    <link rel="stylesheet" href="css/table.css" type="text/css" />
</head>
<body>
<?php
/// PROCEDURA
// CALL GetCustomers();
$host = '127.0.0.1:8889';
$dbname = 'pharmin_skolenie_18_12_2018';
$username = 'root';
$password = 'root';

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // execute the stored procedure
    $sql = 'CALL GetCustomers()';
    // call the stored procedure
    $q = $pdo->query($sql);
    $q->setFetchMode(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    die("Error occurred:" . $e->getMessage());
}
?>
<table border="1">
    <tr>
        <th>Customer Name</th>
        <th>Credit Limit</th>
    </tr>
    <?php while ($r = $q->fetch()): ?>
        <tr>
            <td><?php echo $r['customerName'] ?></td>
            <td><?php echo $r['creditlimit'] ?></td>
        </tr>
    <?php endwhile; ?>
</table>
</body>
</html>

<?php

// Vytvorenie procedury
/*DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetCustomers`()
BEGIN
 SELECT customerName, creditlimit
 FROM customers;
    END;;
DELIMITER ; */

// CALL GetCustomerLevel(2222,@level);
// SELECT @level AS level;
