<?php

/**
 * Get customer level
 * @param int $customerNumber
 * @return string
 */
function getCustomerLevel($customerNumber) {

    $host = '127.0.0.1:8889';
    $dbname = 'pharmin_skolenie_18_12_2018';
    $username = 'root';
    $password = 'root';

    try {
        // Connect do MYSQL
        $pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

        // calling stored procedure command
        $sql = 'CALL GetCustomerLevel(:id,@level)';

        // prepare for execution of the stored procedure
        $stmt = $pdo->prepare($sql);

        // pass value to the command
        $stmt->bindParam(':id', $customerNumber, PDO::PARAM_INT);

        // execute the stored procedure
        $stmt->execute();

        $stmt->closeCursor();

        // execute the second query to get customer's level
        $row = $pdo->query("SELECT @level AS level")->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $row !== false ? $row['level'] : null;
        }
    } catch (PDOException $e) {
        die("Error occurred:" . $e->getMessage());
    }
    return null;
}

/*
CALL GetCustomerLevel(2222,@level);
SELECT @level AS level;

// Vytvorenie procedury
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetCustomerLevel`(
    in  p_customerNumber int(11),
    out p_customerLevel  varchar(10))
BEGIN
    DECLARE creditlim double;

    SELECT creditlimit INTO creditlim
    FROM customers
    WHERE customerNumber = p_customerNumber;

    IF creditlim > 50000 THEN
    SET p_customerLevel = 'PLATINUM';
    ELSEIF (creditlim <= 50000 AND creditlim >= 10000) THEN
        SET p_customerLevel = 'GOLD';
    ELSEIF creditlim < 10000 THEN
        SET p_customerLevel = 'SILVER';
    END IF;

END;;
DELIMITER ;


 */

$customerNo = 5555;
echo sprintf('Customer #%d is %s', $customerNo, getCustomerLevel($customerNo));