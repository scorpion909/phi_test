<?php
session_start();

unset($_SESSION['auth']);
session_destroy();
session_unset();

header('Location: sessions.php');
exit;