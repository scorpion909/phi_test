<?php

class Validator {
    
    protected $rules;
    protected $data;
    protected $errors = [];
    
    public function __construct($rules, $data)
    {
        $this->rules = $rules;
        $this->data = $data;
    }
    
    protected function validateRequired($field, $value) {
        if(!strlen($value)) {
            $this->errors[] = 'Zadajte do pola '.$field.' nejaku hodnotu';
            return false;
        }
        
        return true;
    }

    protected function validateEmail($field, $value) {
        // Emailova validacia
        $this->errors[] = $field.' nie je email';
        return false;
    }

    public function validate() {

        foreach ($this->rules as $field => $rule) {

            if(isset($this->data[$field])) {
                // treba validovat
                $value = $this->data[$field];

                $rulesArr = explode('|', $rule);

                foreach ($rulesArr as $ruleItem) {
                    switch ($ruleItem) {
                        case 'required':
                            $this->validateRequired($field, $value);
                        break;
                        case 'email':
                            $this->validateEmail($field, $value);
                        break;
                        default:
                            throw new Exception('Neznama podmienka validacie');
                        break;
                    }
                }

            }

        }
        
        return [
            'valid' => (count($this->errors)) ? 0 : 1,
            'errors' => $this->errors
        ];
    }


    private function dd($what, $w2 = null) {
        echo "<pre>";
        var_dump($what);
        if($w2) {
            var_dump($w2);
        }
        echo "</pre>";
        exit;
    }

}