<?php
echo "<pre>";
// Vrati systemovu cestu k suboru, ktory je aktualne spracuvany {tentosubor}.php
///var_dump(__DIR__);exit;
echo "</pre>";
// Otvori subor
//$myfile = fopen(__DIR__."/test.txt", "r") or die("Unable to open file!");

// Vypise obsah suboru
//echo fread($myfile,filesize("test.txt"));

// Zatvori otvoreny subor
//fclose($myfile);

// Zapis do suboru
$myfile = fopen("test.txt", "r+") or die("Unable to open file!");
// Precitaj obsah suboru
if(filesize('test.txt') > 0) {
    echo fread($myfile,filesize("test.txt"));
    echo "<hr>";
}

// Zapis do suboru
$txt = "John Doe\nMilan Trieska";
fwrite($myfile, $txt);

// Vypis novy obsah suboru
if(filesize('test.txt') > 0) {
    $myfile = fopen(__DIR__."/test.txt", "r") or die("Unable to open file!");
    echo fread($myfile,filesize("test.txt"));
}

// Zatvor subor
fclose($myfile);