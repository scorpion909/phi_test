<?php

include "vendor/autoload.php";

class Phone {

    private $brand;
    public $number;

    public function __construct($defaultBrand = 'Apple')
    {
        //
        $this->brand = $defaultBrand;
    }

    public function getBrand() {
        return $this->brand;
    }

    public function setBrand($brand) {
        $this->brand = $brand;
    }
    
}

class Mobile extends Phone {

    public $touchscreen;
    public $battery = '1';

    public function __construct()
    {
        parent::__construct();
    }
}

echo "<pre>";

//$p = new Phone();
//$p->brand = 'Apple';
//
//$p2 = new Phone();
//$p2->brand = 'Samsung';
//
//$p->getBrand();
//$p2->getBrand();

//$p = new Phone('Samsung');

$p = new Mobile();
$p->touchscreen = 1;
$p->number = '0903194842';
$p->setBrand('Samsung');
var_dump($p);


//var_dump($p);

$time = \Carbon\Carbon::now();
$time = new \Carbon\Carbon('now');

$d = '2018-11-11';
$time = \Carbon\Carbon::parse($d);
var_dump($time);